#include "f.h"

void sortowanie(struct POOOl a[], int b);

void sortowanie(struct POOOl a[], int b) 
{
    int zmienna_1=0;
    int zmienna_2=0;
    
    for(int i=1; i<b; i++)
    {
        zmienna_1=a[i].x;
        zmienna_2=i-1;

        while(zmienna_2>=0 && a[zmienna_2].x>zmienna_1)
        {
            a[zmienna_2+1].x=a[zmienna_2].x;
            --zmienna_2;
        }
        a[zmienna_2+1].x=zmienna_1;
    }

     for(int i=1; i<b; i++)
    {
        zmienna_1=a[i].y;
        zmienna_2=i-1;

        while(zmienna_2>=0 && a[zmienna_2].y>zmienna_1)
        {
            a[zmienna_2+1].y=a[zmienna_2].y;
            --zmienna_2;
        }
        a[zmienna_2+1].y=zmienna_1;
    }

     for(int i=1; i<b; i++)
    {
        zmienna_1=a[i].rho;
        zmienna_2=i-1;

        while(zmienna_2>=0 && a[zmienna_2].rho>zmienna_1)
        {
            a[zmienna_2+1].rho=a[zmienna_2].rho;
            --zmienna_2;
        }
        a[zmienna_2+1].rho=zmienna_1;
    }

return; 
}