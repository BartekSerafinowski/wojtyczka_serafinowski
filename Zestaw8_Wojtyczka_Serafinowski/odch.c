#include "f.h"

void odch(struct POOOl a[], int b) ;

void odch(struct POOOl a[], int b) 
{
    float dodawanie=0.0;
    float srednia_tymczasowa=0.0;
    float odch_tymczasowe=0.0;

    for(int i=0; i<b; i++)
    { 
        dodawanie=dodawanie+a[i].x;
    }
    srednia_tymczasowa=dodawanie/(b);

    for(int i=0; i<b; i++)
    {
        odch_tymczasowe=odch_tymczasowe+pow(a[i].x-srednia_tymczasowa, 2);       
    }
    a[b+3].x=sqrt(odch_tymczasowe/(b));
    dodawanie=0.0;
    srednia_tymczasowa=0.0;
    odch_tymczasowe=0.0;

    for (int i=0; i<b; i++)
    { 
        dodawanie=dodawanie+a[i].y;
    }
    srednia_tymczasowa=dodawanie/(b);

    for (int i=0; i<b; i++)
    {
        odch_tymczasowe=odch_tymczasowe+pow(a[i].y-srednia_tymczasowa, 2);       
    }
    a[b+3].y=sqrt(odch_tymczasowe/(b));
    dodawanie=0.0; 
    srednia_tymczasowa=0.0;
    odch_tymczasowe=0.0;

    for (int i=0; i<b; i++)
    { 
        dodawanie=dodawanie+a[i].rho;
    }
    srednia_tymczasowa=dodawanie/(b);

    for(int i=0; i<b; i++)
    {
        odch_tymczasowe=odch_tymczasowe+pow(a[i].rho-srednia_tymczasowa, 2);      
    }
    a[b+3].rho=sqrt(odch_tymczasowe/(b));

return;
}