#include "f.h"
#include<math.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main()
{

    FILE *pliczek;
    pliczek=fopen("P0001_attr.rec", "a+");
    
    int b=50;
    struct POOOl *a;
    a=(struct POOOl*)malloc((b+3) *sizeof(struct POOOl)); 

    char lp[50]=" ";
    fgets(lp, 50, pliczek);

    int dopisz=entery(pliczek);

    /* 
    Poniżej jest wpisywanie wartości do struktur 
    */

    for(int i=0; i<b; i++)
    {
        fscanf(pliczek, "L.p. %f, X %f, Y %f, RHO %f", &a[i].lp, &a[i].x, &a[i].y, &a[i].rho);
    }

    sortowanie(a, b);
    mediana(a, b);
    srednia(a, b);
    odch(a, b);    
    
    float srednia_1 = a[b+1].x;
    float srednia_2 = a[b+1].y;
    float srednia_3 = a[b+1].rho;
    float mediana_1 = a[b+2].x;
    float mediana_2 = a[b+2].y;
    float mediana_3 = a[b+2].rho;
    float odch_1 = a[b+3].x;
    float odch_2 = a[b+3].y;
    float odch_3 = a[b+3].rho;

    printf("Srednia: %f %f %f \n",  srednia_1, srednia_2, srednia_3);
    printf("Mediana: %f %f %f \n", mediana_1, mediana_2, mediana_3);
    printf("Odchylenie: %f %f %f \n", odch_1, odch_2, odch_3);

    if(dopisz==50)
    {
        fprintf(pliczek, "Srednia: %f %f %f", srednia_1, srednia_2, srednia_3);
        fprintf(pliczek, "Mediana: %f %f %f", mediana_1, mediana_2, mediana_3);
        fprintf(pliczek, "Odchylenie: %f %f %f", odch_1, odch_2, odch_3);
    }
    

    free(a);
    fclose(pliczek);

return 0;
}

int entery(FILE *plik)
{
    plik=fopen("P0001_attr.rec", "a+");

    int wiersz=0;
    char linijka=' ';
    
    for (linijka=getc(plik); linijka!=EOF; linijka=getc(plik))
    { 
        if (linijka=='\n')
            wiersz++;
    }

    fclose(plik);

return wiersz;
}